if __name__ == '__main__':
    """
    CommandLine:
        python -m watch --help
    """
    from watch.cli.__main__ import main
    main()
